import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';

const Home = lazy(() => import('./components/pages/Home'));
const Login = lazy(() => import('./components/pages/Login'));
const SignUp = lazy(() => import('./components/pages/SignUp'));

const Trials = lazy(() => import('./components/pages/Trials'));
const HowItWorks = lazy(() => import('./components/pages/HowItWorks'));
const Terms = lazy(() => import('./components/pages/Terms'));
const Privacy = lazy(() => import('./components/pages/Privacy'));
const Contact = lazy(() => import('./components/pages/Contact'));

function App() {
  return (
    <div>
      <Router>
        <Suspense fallback={<div></div>}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={SignUp} />

            <Route exact path="/trials" component={Trials} />
            <Route exact path="/howitworks" component={HowItWorks} />

            <Route exact path="/terms" component={Terms} />
            <Route exact path="/privacy" component={Privacy} />
            <Route exact path="/contact" component={Contact} />
          </Switch>
        </Suspense>
      </Router>
    </div>
  );
}

export default App;
