import React, { useState, useEffect } from 'react';
import './styles.scss';
import NavBar from '../../common/NavBar';
import HeroVoteBox from '../../common/HeroVoteBox';
import ClosingBar from '../../common/ClosingBar';
import VoteBox from '../../common/VoteBox';
import Footer from '../../common/Footer';
import axios from 'axios';

import CloseIcon from '@material-ui/icons/Close';
import config from '../../../config';

const people = [
  {
    _id: 'Kanye West',
    name: 'Kanye West',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.',
    area: 'Entertainment',
    image: 'kanye',
    upVotes: 64,
    downVotes: 36,
  },
  {
    _id: 'Mark Zuckerberg',
    name: 'Mark Zuckerberg',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.',
    area: 'Business',
    image: 'mark',
    upVotes: 36,
    downVotes: 64,
  },
  {
    _id: 'Cristina Fernández de Kirchner',
    name: 'Cristina Fernández de Kirchner',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.',
    area: 'Politics',
    image: 'cristina',
    upVotes: 36,
    downVotes: 64,
  },
  {
    _id: 'Malala Yousafzai',
    name: 'Malala Yousafzai',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.',
    area: 'Entertainment',
    image: 'malala',
    upVotes: 64,
    downVotes: 36,
  },
];

function Home() {
  const [boxes, setBoxes] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const result = await axios.get(`${config.apiUrl}/box`);
        setBoxes(result.data.items);
      } catch (error) {
        //fallback
        setBoxes(people);
        console.error(error);
      }
    }
    fetchData();
  }, []);

  function renderVoteBoxes() {
    if (boxes.length > 0) {
      return boxes.map((person) => (
        <VoteBox key={person._id} person={person} />
      ));
    }
    return [];
  }

  return (
    <div className="home-container">
      <div className="hero">
        <NavBar />
        <div className="box">
          <HeroVoteBox />
        </div>
        <ClosingBar />
      </div>

      <div className="home-content page-padding">
        <div className="info">
          <div className="left">
            <p>Speak out. Be heard.</p>
            <p>Be counted</p>
          </div>
          <div className="middle">
            Rule of Thumb is a crowd sourced court of public opinion where
            anyone and everyone can speak out and speak freely. It’s easy: You
            share your opinion, we analyze and put the data in a public report.
          </div>
          <div className="right">
            <CloseIcon fontSize="large" />
          </div>
        </div>
        <h2>Votes</h2>
        <div className="previous-rulings">{renderVoteBoxes()}</div>
        <div className="add-people">
          <div className="colored-background">
            <span>Is there anyone else you would want us to add?</span>
            <a href=" #">Submit a Name</a>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default Home;
