import React, { useState } from 'react';
import axios from 'axios';
import config from '../../../config';
import { Link } from 'react-router-dom';

import { setCookie } from '../../../utils/cookies';
import { useHistory } from 'react-router-dom';

import './styles.scss';

const { apiUrl, cookieName, domainName } = config;

function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const history = useHistory();

  async function handleLogin(e) {
    e.preventDefault();
    try {
      const result = await axios.post(`${apiUrl}/token`, { email, password });

      setCookie({
        name: cookieName,
        value: result.data.token,
        expirationDays: 15,
        domain: domainName,
      });

      history.push('/');
    } catch (error) {
      if (error.message.includes('400') || error.message.includes('404')) {
        setError('Invalid Email / Password.');
      } else {
        setError('Internal Server Error');
      }
    }
  }

  function handleInputChange(e) {
    setError('');
    switch (e.target.name) {
      case 'email':
        const email = e.target.value;
        setEmail(email);
        break;
      case 'password':
        const password = e.target.value;
        setPassword(password);
        break;
      default:
        break;
    }
  }

  return (
    <div className="login-form">
      <h2>Login</h2>
      <form onSubmit={handleLogin}>
        <div className="formGroup">
          <label>Email</label>
          <input
            name="email"
            type="email"
            value={email}
            onChange={handleInputChange}
            placeholder="email@email.com"
          ></input>
        </div>
        <div className="formGroup">
          <label>Password</label>
          <input
            name="password"
            type="password"
            value={password}
            onChange={handleInputChange}
          ></input>
        </div>

        <span>{error}</span>

        <div className="formGroup">
          <input type="submit" value="Login" />
        </div>
      </form>

      <div className="formGroup">
        <p>You don't have an account yet?</p>
        <p>
          <Link to="/signup">Sign Up</Link> or <Link to="/">Go Back</Link>
        </p>
      </div>
    </div>
  );
}

export default Login;
