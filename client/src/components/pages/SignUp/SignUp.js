import React, { useState } from 'react';
import axios from 'axios';
import config from '../../../config';
import { Link } from 'react-router-dom';

import './styles.scss';

function SignUp() {
  const [accountCreated, setAccountCreated] = useState(false);
  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [age, setAge] = useState('');
  const [marriageStatus, setMarriageStatus] = useState('');

  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  async function handleUserCreation(e) {
    e.preventDefault();
    try {
      await axios.post(`${config.apiUrl}/user`, {
        firstName,
        lastName,
        age,
        marriageStatus,
        email,
        password,
      });

      setAccountCreated(true);
    } catch (error) {
      if (error.message.includes('400')) {
        setError('All the fields are required');
      } else {
        setError('Internal Server Error');
      }
    }
  }

  function handleInputChange(e) {
    setError('');
    switch (e.target.name) {
      case 'email':
        const email = e.target.value;
        setEmail(email);
        break;
      case 'firstName':
        const firstName = e.target.value;
        setFirstName(firstName);
        break;
      case 'lastName':
        const lastName = e.target.value;
        setLastName(lastName);
        break;
      case 'age':
        const age = e.target.value;
        setAge(age);
        break;
      case 'marriageStatus':
        const marriageStatus = e.target.value;
        setMarriageStatus(marriageStatus);
        break;
      case 'password':
        const password = e.target.value;
        setPassword(password);
        break;
      default:
        break;
    }
  }

  return (
    <div className="signup">
      <h2>Account Creation</h2>
      {accountCreated ? (
        <div>
          Your account has been created. Please<Link to="Login">Login</Link>
        </div>
      ) : (
        <form onSubmit={handleUserCreation}>
          <div className="formGroup">
            <label>Email</label>
            <input
              name="email"
              type="email"
              value={email}
              onChange={handleInputChange}
              placeholder="email@email.com"
              required
            ></input>
          </div>

          <div className="formGroup">
            <label>First Name</label>
            <input
              name="firstName"
              type="firstName"
              value={firstName}
              onChange={handleInputChange}
              placeholder="Jhon"
              required
            ></input>
          </div>

          <div className="formGroup">
            <label>Last Name</label>
            <input
              name="lastName"
              type="lastName"
              value={lastName}
              onChange={handleInputChange}
              placeholder="Doe"
              required
            ></input>
          </div>

          <div className="formGroup">
            <label>Age</label>
            <input
              name="age"
              type="age"
              value={age}
              onChange={handleInputChange}
              placeholder="20"
              required
            ></input>
          </div>

          <div className="formGroup">
            <label>Marriage Status</label>
            <input
              name="marriageStatus"
              type="marriageStatus"
              value={marriageStatus}
              onChange={handleInputChange}
              placeholder="single | married | widower"
              required
            ></input>
          </div>

          <div className="formGroup">
            <label>Password</label>
            <input
              name="password"
              type="password"
              value={password}
              onChange={handleInputChange}
              required
            ></input>
          </div>

          <span>{error}</span>

          <div className="formGroup">
            <input type="submit" value="Sign Up" />
          </div>

          <div className="formGroup">
            <p>
              <Link to="/login">Go Back</Link>
            </p>
          </div>
        </form>
      )}
    </div>
  );
}

export default SignUp;
