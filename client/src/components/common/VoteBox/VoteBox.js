import React, { useState, useEffect } from 'react';
import './styles.scss';
import PercentageBar from '../PercentageBar';
import classNames from 'classnames';
import axios from 'axios';
import config from '../../../config';
import { getCookie } from '../../../utils/cookies';

import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import CristinaImg from './assets/cristina.jpg';
import KanyeImg from './assets/kanye.jpg';
import MarkImg from './assets/mark.jpg';
import MalalaImg from './assets/malala.jpg';

const { cookieName, domainName, apiUrl } = config;

function returnImg(string) {
  switch (string) {
    case 'cristina':
      return CristinaImg;
    case 'kanye':
      return KanyeImg;
    case 'mark':
      return MarkImg;
    case 'malala':
      return MalalaImg;
    default:
      break;
  }
}

function VoteBox(props) {
  const { person } = props;

  const {
    _id = 'id',
    name = 'First Last',
    description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.',
    area = 'Entertainment',
    image = 'kanye',
  } = person;

  const [upVotes, setUpVotes] = useState(0);
  const [downVotes, setDownVotes] = useState(0);

  const [cookie, setCookie] = useState('');
  const [selectedThumb, setSelectedThumb] = useState('');
  const [voteSuccess, setVoteSuccess] = useState(false);
  const [pickThumb, setPickThumb] = useState(false);
  const [voteError, setVoteError] = useState(false);
  const [voteExceeded, setVoteExceeded] = useState(false);
  const [requireLogin, setRequireLogin] = useState(false);

  useEffect(() => {
    const cookie = getCookie(cookieName, domainName);
    setUpVotes(person.upVotes);
    setDownVotes(person.downVotes);
    setCookie(cookie);
  }, [person]);

  async function fetchVotes() {
    try {
      const result = await axios.get(`${apiUrl}/box?searchKey=${_id}`);
      const box = result.data.items[0];
      setUpVotes(box.upVotes);
      setDownVotes(box.downVotes);
    } catch (error) {}
  }

  function handleSelectThumb(e) {
    e.preventDefault();
    setPickThumb(false);
    setSelectedThumb(e.target.name);
  }

  async function handleVoteNow(e) {
    e.preventDefault();

    if (selectedThumb === '') {
      setPickThumb(true);
      return;
    }

    try {
      await axios.post(
        `${apiUrl}/vote`,
        {
          idBox: _id,
          thumb: selectedThumb,
        },
        {
          headers: {
            Authorization: `Bearer ${cookie}`,
          },
        }
      );
      setVoteSuccess(true);
      await fetchVotes();
    } catch (error) {
      if (error.message.includes('401')) {
        setRequireLogin(true);
        return;
      }
      if (error.message.includes('400')) {
        setVoteExceeded(true);
        return;
      }
      setVoteError(true);
    }
  }

  function handleVoteAgain(e) {
    e.preventDefault();
    setVoteSuccess(false);
    setSelectedThumb('');
  }

  return (
    <div className="vote-box">
      <img src={returnImg(image)} alt={image} />
      <div className="content">
        <div className="person-info">
          <div className="title">
            {upVotes >= downVotes ? (
              <div className="up-voted">
                <ThumbUpIcon />
              </div>
            ) : (
              <div className="down-voted">
                <ThumbDownIcon />
              </div>
            )}
            <h2>{name}</h2>
          </div>
          <div className="right">
            <span>
              <b>1 month ago</b> in {area}
            </span>

            {voteSuccess ? <p> Thank you for Voting!</p> : <p>{description}</p>}
            {requireLogin && (
              <p className="alert-message">
                You need to login in order to Vote!
              </p>
            )}
            {voteError && (
              <p className="alert-message">
                Ups, something went wrong, try to vote again later!
              </p>
            )}
            {pickThumb && (
              <p className="alert-message">
                You must pick a Thumb in order to Vote!
              </p>
            )}
            {voteExceeded && (
              <p className="alert-message">
                You have already voted 3 times for this box!
              </p>
            )}

            {!voteSuccess ? (
              <div className="voting-buttons">
                <a
                  name="up"
                  href=" #"
                  className={classNames('up-voted', {
                    selected: selectedThumb === 'up',
                  })}
                  onClick={handleSelectThumb}
                >
                  <ThumbUpIcon />
                </a>
                <a
                  name="down"
                  href=" #"
                  className={classNames('down-voted', {
                    selected: selectedThumb === 'down',
                  })}
                  onClick={handleSelectThumb}
                >
                  <ThumbDownIcon />
                </a>
                <a className="border-button" href=" #" onClick={handleVoteNow}>
                  Vote now
                </a>
              </div>
            ) : (
              <div className="voting-buttons">
                <a
                  className="border-button"
                  href=" #"
                  onClick={handleVoteAgain}
                >
                  Vote Again
                </a>
              </div>
            )}
          </div>
        </div>

        <PercentageBar downVotes={downVotes} upVotes={upVotes} />
      </div>
    </div>
  );
}

export default VoteBox;
