import React, { useState, useEffect, useCallback } from 'react';
import './styles.scss';

import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';

function PercentageBar(props) {
  const { upVotes, downVotes } = props;
  const [upVotesPercentage, setUpVotesPercentage] = useState(50);
  const [downVotesPercentage, setDownVotesPercentage] = useState(50);

  const calculatePercentage = useCallback(() => {
    const total = upVotes + downVotes;
    if (total === 0) {
      return;
    }
    const upPercentage = Math.ceil((upVotes * 100) / total);
    setUpVotesPercentage(upPercentage);
    setDownVotesPercentage(100 - upPercentage);
  }, [upVotes, downVotes]);

  useEffect(() => {
    calculatePercentage();
  }, [calculatePercentage]);

  return (
    <div className="percentage-bar">
      {upVotesPercentage > 0 && (
        <div className="up-vote-bar" style={{ width: `${upVotesPercentage}%` }}>
          <ThumbUpIcon /> {upVotesPercentage}%
        </div>
      )}
      {downVotesPercentage > 0 && (
        <div
          className="down-vote-bar"
          style={{ width: `${downVotesPercentage}%` }}
        >
          {downVotesPercentage}% <ThumbDownIcon />
        </div>
      )}
    </div>
  );
}

export default PercentageBar;
