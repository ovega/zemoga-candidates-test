import React from 'react';
import './styles.scss';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import LinkIcon from '@material-ui/icons/Link';

function HeroVoteBox(props) {
  const {
    name = `Pope Francis`,
    summary = `He’s talking tough on clergy sexual abuse, but is he just another
  papal pervert protector? (thumbs down) or a true pedophile punishing
  pontiff? (thumbs up)`,
    link = ` #`,
  } = props;
  return (
    <div className="hero-vote-box">
      <div className="body">
        <div className="body-title">
          <p>What's your opinion on</p>
          <span>{name}?</span>
        </div>
        <div className="body-content">
          <p>{summary}</p>
        </div>
        <div className="body-link">
          <LinkIcon />
          <a href={link}>More Information</a>
        </div>
        <span>What's your Verdict?</span>
      </div>
      <div className="verdict-box">
        <div className="up-vote">
          <ThumbUpIcon fontSize="large" />
        </div>
        <div className="down-vote">
          <ThumbDownIcon fontSize="large" />
        </div>
      </div>
    </div>
  );
}

export default HeroVoteBox;
