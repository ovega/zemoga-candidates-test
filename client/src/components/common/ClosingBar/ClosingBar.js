import React from 'react';
import './styles.scss';

function ClosingBar(props) {
  const { days = 22 } = props;
  return (
    <div className="closing-bar">
      <div className="closing">
        CLOSING IN
        <div className="arrow-right"></div>
      </div>
      <div className="days">
        <span>{days}</span> days
      </div>
    </div>
  );
}

export default ClosingBar;
