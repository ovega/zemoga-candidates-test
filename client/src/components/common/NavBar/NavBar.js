import React, { useEffect, useState } from 'react';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from 'react-router-dom';
import { getCookie, deleteCookie } from '../../../utils/cookies';
import config from '../../../config';
import { useHistory } from 'react-router-dom';

import './styles.scss';

const { cookieName, domainName } = config;

function NavBar() {
  const [cookie, setCookie] = useState('');
  const history = useHistory();
  useEffect(() => {
    const cookie = getCookie(cookieName);
    setCookie(cookie);
  }, []);

  function handleLogout(e) {
    e.preventDefault();
    deleteCookie(cookieName, domainName);
    history.push('/login');
  }

  return (
    <header className="navbar page-padding">
      <div className="brand">
        <Link to="/">
          <span>Rule of Thumb.</span>
        </Link>
      </div>
      <nav className="navigation">
        <ul>
          <li>
            <Link to="/trials">Past Trials</Link>
          </li>
          <li>
            <Link to="/howitworks">How it Works</Link>
          </li>
          <li>
            {cookie !== '' ? (
              <a href=" #" onClick={handleLogout}>
                Logout
              </a>
            ) : (
              <Link to="/login">Login / Sign Up</Link>
            )}
          </li>
          <li>
            <SearchIcon fontSize="large" />
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default NavBar;
