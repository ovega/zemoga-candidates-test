import React from 'react';
import { Link } from 'react-router-dom';
import './styles.scss';

import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';

function Footer() {
  return (
    <div className="footer">
      <nav className="navigation">
        <ul>
          <li>
            <Link to="/terms">Terms and Conditions</Link>
          </li>
          <li>
            <Link to="/privacy">Privacy Policy</Link>
          </li>
          <li>
            <Link to="/contact">Contact Us</Link>
          </li>
        </ul>
      </nav>
      <div className="social-media">
        <span>Follow Us</span>
        <FacebookIcon fontSize="large" />
        <TwitterIcon fontSize="large" />
      </div>
    </div>
  );
}

export default Footer;
