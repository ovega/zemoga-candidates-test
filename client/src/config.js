const {
  REACT_APP_ENV = 'development',
  REACT_APP_AUTH_COOKIE = 'devCookie',
  REACT_APP_DOMAIN = 'localhost:3000',
} = process.env;

let apiUrl;

switch (REACT_APP_ENV) {
  case 'development':
    apiUrl = 'http://localhost:3000';
    break;
  default:
    apiUrl = 'https://personalize-prototype.test.cebroker.com/api';
    break;
}

const config = {
  apiUrl,
  cookieName: REACT_APP_AUTH_COOKIE,
  domainName: REACT_APP_DOMAIN,
};

export default config;
