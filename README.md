# zemoga-candidates-test

## Run the project 

1. Clone this repository in your local machine. It contains two folders called api (backend) & client (frontend).

2. Go to the api folder and install npm dependencies

```
cd /api
npm i
```

3. Go to the client folder and install npm dependencies

```
cd /client
npm i
```

4. You can run this project via pm2 or running each app in console (api, client), the api will run on port 3000 and the client on the port 8080.

    **If you have pm2 installed the command below.**

    ```
    pm2 start pm2.process.yml
    ```

    **If you prefer to run each app individually use the commands bellow.**

    ```
    cd /api
    npm start
    ```

    ```
    cd /client
    npm start
    ```

    **Use these URLS To access the apps**

    - http://localhost:3000 [ api ]
    - http://localhost:8080 [ client ]
    
## Frontend Considerations

The application was built using React.js

- With a bit more time I would implemented a loading status for the VoteBoxes component also would prefer to load the boxes' images from an images storage (right now they are assets of the VoteBox component)
- All images were exported from Photoshop.
- Material UI icons where used for some icons (Thumbs | Wiki Link)
- All requirements were met:
    - [Layout HTML-CSS](https://github.com/zemoga/ui-test#layout-html-css)
    - [interaction - JS](https://github.com/zemoga/ui-test#interaction---js)

## Backend Considerations

The application was build using Node.js (v12.18.0) and Express.js.

- I include the .env file in the repo to allow you to test this faster. I know it is a bad practice.
- All the requirements were met:
    - [NodeJS](https://github.com/zemoga/ui-test#nodejs)

### Endpoints

All the enpoints can be found in this [Postman Collection](https://storage.cebroker.com/cebroker/df4f1c7a-0607-4909-b7df-47f8853eec14)

To use the endpoints that require Auth, you need to use the `/token` endpoint, provide a valid `emaild` and `password`, and then use the retrieved token as the header of the request.

```
// Auth Header Example
Authorization: Bearer {retrievedToken}
```

| Endpoint | Method | Parameters | Auth Required | Comments |  
| ------ | ------ | ------ | ------ | ------ |
| /user | POST | _id, firstName, lastName, age, marriageStatus, email, password | no | If the **_id** is provided the user will be updated |
| /user | GET | currentPage, limit, firstName, lastName, email, searchKey | yes | If the **searchKey** is provided it will return the users that match the criteria by  firstName, lastName or email |
| /user | DEL | _id | yes | It will delete the user but no the votes |
| /box | POST | name, description, area, image | yes | It will add a new box  (The images are hard coded) |
| /box | GET | currentPage, limit, name, area, searchKey | no | If the **searchKey** is provided it will return the boxes that match the criteria by  name or area |
| /vote | POST | idBox, thumb | yes | It will add a new vote related to the user who submits it |
| /vote | GET | currentPage, limit, idUser, idBox | yes | List the votes by user, by box or by both |
| /token | POST | email, password | no | Return the auth token used to call secured endpoints |
