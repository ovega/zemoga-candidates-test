'use strict';

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

const getCredentials = () => {
  const {
    MONGO_HOST,
    MONGO_PORT,
    MONGO_DATABASE,
    MONGO_USER,
    MONGO_PASSWORD,
    MONGO_REPLICASET,
    MONGO_SSL,
    MONGO_AUTH_SOURCE
  } = process.env;

  let replicas = MONGO_HOST.split(',')
    .map(url => `${url}:${MONGO_PORT}`)
    .join(',');

  const MONGO_URL = `mongodb://${replicas}/${MONGO_DATABASE}`;
  return {
    MONGO_URL,
    MONGO_USER,
    MONGO_PASSWORD,
    MONGO_REPLICASET,
    MONGO_SSL,
    MONGO_AUTH_SOURCE
  };
};

const connect = async () => {
  const mongoCredentials = helper.getCredentials();
  console.info(`Connecting to mongo database \n`);

  try {
    await mongoose.connect(mongoCredentials.MONGO_URL, {
      user: mongoCredentials.MONGO_USER,
      pass: mongoCredentials.MONGO_PASSWORD,
      replicaSet: mongoCredentials.MONGO_REPLICASET,
      ssl: mongoCredentials.MONGO_SSL == '1' ? true : false,
      authSource: mongoCredentials.MONGO_AUTH_SOURCE,
      reconnectTries: 30000,
      reconnectInterval: 1000,
      useNewUrlParser: true
    });

    console.info('Connected successfully to mongodb \n');

    const connection = mongoose.connection;
    connection.on('connecting', () => console.info('Connecting to mongodb'));
    connection.on('disconnecting', () =>
      console.info('Disconnecting from mongodb')
    );
    connection.on('disconnected', () =>
      console.info('Disconnected from mongodb')
    );
    connection.on('close', () => console.info('Mongodb connection closed'));
    connection.on('error', err =>
      console.info('Error Connecting to mongodb', err.message)
    );
    connection.on('reconnected', () =>
      console.info('Mongodb reconnected successfully')
    );
    return true;
  } catch (err) {
    console.info('Mongodb connection error', JSON.stringify(err));
    return false;
  }
};

const isConnected = () => {
  return mongoose.connection.readyState === 1 ? true : false;
};

const helper = {
  getCredentials,
  connect,
  isConnected
};

module.exports = helper;
