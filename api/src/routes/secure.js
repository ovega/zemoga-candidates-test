'use strict';

const express = require('express');
const router = express.Router();

// USER
const userRouter = express.Router();
const userController = require('../controllers/user');
userRouter.put('/', userController.set);
userRouter.get('/', userController.get);
userRouter.delete('/', userController.del);

// box
const boxRouter = express.Router();
const boxController = require('../controllers/box');
boxRouter.post('/', boxController.post);
boxRouter.get('/', boxController.get);

// VOTE
const voteRouter = express.Router();
const voteController = require('../controllers/vote');
voteRouter.post('/', voteController.post);
voteRouter.get('/', voteController.get);

router.use('/user', userRouter);
router.use('/box', boxRouter);
router.use('/vote', voteRouter);

module.exports = router;
