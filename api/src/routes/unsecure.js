'use strict';

const express = require('express');
const router = express.Router();

const indexRouter = express.Router();
indexRouter.get('/', function (req, res) {
  res.status(200).json({
    response: `Internal API is working properly.`,
  });
});

//TOKEN
const tokenRouter = express.Router();
const tokenController = require('../controllers/token');
tokenRouter.post('/', tokenController.post);

//USER
const userRouter = express.Router();
const userController = require('../controllers/user');
userRouter.post('/', userController.set);

//box
const boxRouter = express.Router();
const boxController = require('../controllers/box');
boxRouter.get('/', boxController.get);

router.use('/', indexRouter);
router.use('/token', tokenRouter);
router.use('/user', userRouter);
router.use('/box', boxRouter);

module.exports = router;
