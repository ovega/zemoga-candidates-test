'use strict';

const secure = require('./secure');
const unsecure = require('./unsecure');

module.exports = {
  secure,
  unsecure
};
