'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment-timezone');
moment.tz.setDefault('America/Bogota');

const BoxSchema = new Schema({
  name: {
    type: String,
    required: true,
    lowercase: true,
  },
  description: {
    type: String,
    required: true,
  },
  area: {
    type: String,
    required: true,
    lowercase: true,
  },
  image: {
    type: String,
    required: true,
  },
  dtAdded: {
    type: String,
    default: moment().format(),
  },
});

const Box = mongoose.model('Boxes', BoxSchema);

module.exports = Box;
