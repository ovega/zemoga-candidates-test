'use strict';

const mongoose = require('mongoose');
const moment = require('moment-timezone');
const Schema = mongoose.Schema;
moment.tz.setDefault('America/Bogota');

const VoteSchema = new Schema({
  idUser: {
    type: String,
    required: true,
    lowercase: true,
  },
  idBox: {
    type: String,
    required: true,
    lowercase: true,
  },
  thumb: {
    type: String,
    required: true,
    lowercase: true,
  },
  voteAt: {
    type: String,
    default: moment().format(),
  },
});

const Vote = mongoose.model('Votes', VoteSchema);

module.exports = Vote;
