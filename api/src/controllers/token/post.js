const passport = require('passport');
const jwt = require('jsonwebtoken');
const { SECRET_KEY } = process.env;

async function post(req, res, next) {
  const payload = req.body;

  if (!payload.email || !payload.password) {
    return res.status(400).json({
      message: 'Email and password are required.',
    });
  }

  passport.authenticate('token', async (err, user, info) => {
    try {
      if (err || !user) {
        return res.status(info.status).json({ message: info.message });
      }
      req.login(user, { session: false }, async (err) => {
        if (err) return next(err);
        //We don't want to store the sensitive information such as the
        //user password in the token so we pick only the email and id
        const body = { _id: user._id, role: user.role };
        //Sign the JWT token and populate the payload with the user email and id
        const token = jwt.sign({ user: body }, SECRET_KEY);
        //Send back the token to the user
        return res.status(200).json({ token });
      });
    } catch (err) {
      return next(err);
    }
  })(req, res, next);
}

module.exports = post;
