'use strict';
const BoxModel = require('../../models/mongo/Box');

async function post(req, res) {
  let { name, description, area, image } = req.body;

  if (!name || !description || !area || !image) {
    return res.status(400).json({
      message: 'name, description, area and image are required.',
    });
  }

  try {
    const user = new BoxModel({
      name,
      description,
      area,
      image,
    });
    await user.save();
    return res.status(200).json({
      message: 'Box Created.',
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      message: 'Internal Server Error.',
    });
  }
}

module.exports = post;
