'use strict';
const mongoose = require('mongoose');
const BoxModel = require('../../models/mongo/Box');
const VoteModel = require('../../models/mongo/Vote');

async function get(req, res) {
  const { currentPage = 1, limit = 10, name, area, searchKey } = req.query;

  let totalParams;
  let query = {},
    params = {};

  try {
    if (searchKey) {
      query = {
        $or: [
          { name: RegExp(searchKey.toLowerCase()) },
          { area: RegExp(searchKey.toLowerCase()) },
        ],
      };

      //get by id
      const isValidId = mongoose.Types.ObjectId.isValid(searchKey);
      query = isValidId ? { _id: searchKey } : query;
    } else {
      if (name) params.name = RegExp(name.toLowerCase());
      if (area) params.area = RegExp(area.toLowerCase());

      totalParams = Object.keys(params).length;
      query = totalParams > 1 ? { $or: [params] } : params;
    }

    let totalResults = 0;
    if (totalParams === 0) {
      totalResults = await BoxModel.estimatedDocumentCount();
    } else {
      totalResults = await BoxModel.countDocuments(query, (err, count) => {
        return count;
      });
    }

    const items = await BoxModel.find(query, '', {
      skip: parseInt(currentPage - 1) * parseInt(limit),
      limit: parseInt(limit),
    });

    //Populate Votes
    const boxesWithVotes = [];
    for (let i = 0; i < items.length; i++) {
      const result = await VoteModel.find({ idBox: items[i]._id });
      const total = result.length;
      const totalUp = result.filter((e) => e.thumb === 'up').length;
      const totalDown = total - totalUp;

      const { _id, name, description, area, image } = items[i];
      const box = {
        _id,
        name,
        description,
        area,
        image,
        upVotes: totalUp,
        downVotes: totalDown,
      };
      boxesWithVotes.push(box);
    }

    const result = {
      currentPage: parseInt(currentPage),
      limit: parseInt(limit),
      totalResults: totalResults,
      items: boxesWithVotes,
    };

    const statusCode = result.items.length > 0 ? 200 : 404;
    return res.status(statusCode).json(result);
  } catch (err) {
    if (err.message.includes('Invalid regular expression')) {
      return res.status(400).json({
        message: 'Invalid Search Parameter.',
      });
    }
    console.error(err);
    return res.status(500).json({
      message: 'Internal Server Error.',
    });
  }
}

module.exports = get;
