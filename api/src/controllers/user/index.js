'use strict';

const get = require('./get');
const set = require('./set');
const del = require('./del');

module.exports = {
  get,
  set,
  del
};
