'use strict';
const bcrypt = require('bcryptjs');
const UserModel = require('../../models/mongo/User');

async function set(req, res) {
  let {
    _id,
    firstName,
    lastName,
    age,
    marriageStatus,
    email,
    password,
  } = req.body;

  if (
    !firstName ||
    !lastName ||
    !age ||
    !marriageStatus ||
    !email ||
    !password
  ) {
    return res.status(400).json({
      message:
        'firstName, lastName, age, marriageStatus, email and password are required.',
    });
  }

  const re = /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const isValidEmail = re.test(String(email).toLowerCase());

  if (!isValidEmail) {
    return res.status(400).json({
      message: 'Invalid email format (E.g: email@example.com).',
    });
  }

  try {
    if (!_id) {
      const user = new UserModel({
        firstName,
        lastName,
        age,
        marriageStatus,
        email,
        password,
      });
      await user.save();
      return res.status(200).json({
        message: 'User Created.',
      });
    } else {
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(password, salt);

      await UserModel.findOneAndUpdate(
        { _id },
        {
          firstName,
          lastName,
          age,
          marriageStatus,
          email,
          password: hash,
        }
      );
      return res.status(200).json({
        message: 'User Updated.',
      });
    }
  } catch (error) {
    if (error.message.includes('E11000')) {
      return res.status(400).json({
        message: 'The email provide is already in use.',
      });
    } else {
      console.error(error);
      return res.status(500).json({
        message: 'Internal Server Error.',
      });
    }
  }
}

module.exports = set;
