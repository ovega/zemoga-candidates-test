'use strict';
const UserModel = require('../../models/mongo/User');

async function del(req, res) {
  let { _id } = req.body;

  try {
    await UserModel.findOneAndDelete({ _id });
    return res.status(200).json({
      message: 'User Deleted.',
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      message: 'Internal Server Error.',
    });
  }
}

module.exports = del;
