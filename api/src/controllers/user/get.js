'use strict';
const mongoose = require('mongoose');
const UserModel = require('../../models/mongo/User');

async function get(req, res) {
  const {
    currentPage = 1,
    limit = 10,
    firstName,
    lastName,
    email,
    searchKey,
  } = req.query;

  let totalParams;
  let query = {},
    params = {};

  try {
    if (searchKey) {
      query = {
        $or: [
          { firstName: RegExp(searchKey.toLowerCase()) },
          { lastName: RegExp(searchKey.toLowerCase()) },
          { email: RegExp(searchKey.toLowerCase()) },
        ],
      };

      //get by id
      const isValidId = mongoose.Types.ObjectId.isValid(searchKey);
      query = isValidId ? { _id: searchKey } : query;
    } else {
      if (firstName) params.firstName = RegExp(firstName.toLowerCase());
      if (lastName) params.lastName = RegExp(lastName.toLowerCase());

      if (email) params.email = RegExp(email.toLowerCase());

      totalParams = Object.keys(params).length;
      query = totalParams > 1 ? { $or: [params] } : params;
    }

    let totalResults = 0;
    if (totalParams === 0) {
      totalResults = await UserModel.estimatedDocumentCount();
    } else {
      totalResults = await UserModel.countDocuments(query, (err, count) => {
        return count;
      });
    }

    const items = await UserModel.find(query, '', {
      skip: parseInt(currentPage - 1) * parseInt(limit),
      limit: parseInt(limit),
    });

    const result = {
      currentPage: parseInt(currentPage),
      limit: parseInt(limit),
      totalResults: totalResults,
      items,
    };

    const statusCode = result.items.length > 0 ? 200 : 404;
    return res.status(statusCode).json(result);
  } catch (err) {
    if (err.message.includes('Invalid regular expression')) {
      return res.status(400).json({
        message: 'Invalid Search Parameter.',
      });
    }
    console.error(err);
    return res.status(500).json({
      message: 'Internal Server Error.',
    });
  }
}

module.exports = get;
