'use strict';
const mongoose = require('mongoose');
const VoteModel = require('../../models/mongo/Vote');
const UserModel = require('../../models/mongo/User');
const BoxModel = require('../../models/mongo/Box');

async function post(req, res) {
  const { idBox, thumb } = req.body;
  const { _id } = req.user;
  const idUser = _id;

  if (!idUser || !idBox || !thumb) {
    return res.status(400).json({
      message: 'idBox and thumb are required.',
    });
  }

  const thumbOptions = ['up', 'down'];
  if (!thumbOptions.includes(thumb)) {
    return res.status(400).json({
      message: "Only 'up' or 'down' values are allowed as thumb.",
    });
  }

  try {
    //validate user existence
    if (!mongoose.Types.ObjectId.isValid(idUser)) {
      return res.status(400).json({
        message: 'The provided idUser is not Valid.',
      });
    }

    const userExists = await UserModel.findById(idUser);
    if (!userExists) {
      return res.status(400).json({
        message: 'The provided user does not exists.',
      });
    }

    //validate  box existence
    if (!mongoose.Types.ObjectId.isValid(idBox)) {
      return res.status(400).json({
        message: 'The provide idBox is not Valid.',
      });
    }
    const boxExists = await BoxModel.findById(idBox);

    if (!boxExists) {
      return res.status(400).json({
        message: 'The provide box does not exists.',
      });
    }
    //validate number of votes
    const totalVotes = await VoteModel.find({
      $and: [{ idUser }, { idBox }],
    });

    if (totalVotes.length > 2) {
      return res.status(400).json({
        message: 'You have already vote 3 times for this Box.',
      });
    }

    const user = new VoteModel({
      idUser,
      idBox,
      thumb,
    });
    await user.save();
    return res.status(200).json({
      message: 'Vote Created.',
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      message: 'Internal Server Error.',
    });
  }
}

module.exports = post;
