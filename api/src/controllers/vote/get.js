'use strict';
const mongoose = require('mongoose');
const VoteModel = require('../../models/mongo/Vote');

async function get(req, res) {
  const { currentPage = 1, limit = 10, idUser, idBox } = req.query;

  let totalParams;
  let query = {},
    params = {};

  try {
    if (idUser) params.idUser = RegExp(idUser.toLowerCase());
    if (idBox) params.idBox = RegExp(idBox.toLowerCase());

    totalParams = Object.keys(params).length;
    query = totalParams > 1 ? { $or: [params] } : params;

    let totalResults = 0;
    if (totalParams === 0) {
      totalResults = await VoteModel.estimatedDocumentCount();
    } else {
      totalResults = await VoteModel.countDocuments(query, (err, count) => {
        return count;
      });
    }

    const items = await VoteModel.find(query, '', {
      skip: parseInt(currentPage - 1) * parseInt(limit),
      limit: parseInt(limit),
    });

    const result = {
      currentPage: parseInt(currentPage),
      limit: parseInt(limit),
      totalResults: totalResults,
      items,
    };

    const statusCode = result.items.length > 0 ? 200 : 404;
    return res.status(statusCode).json(result);
  } catch (err) {
    if (err.message.includes('Invalid regular expression')) {
      return res.status(400).json({
        message: 'Invalid Parameter.',
      });
    }
    console.error(err);
    return res.status(500).json({
      message: 'Internal Server Error.',
    });
  }
}

module.exports = get;
