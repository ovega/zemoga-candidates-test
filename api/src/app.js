'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const { secure, unsecure } = require('./routes');

require('./auth/passport');

const app = express();

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});
app.use(cors());
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

// Load api routes
app.use('/', unsecure);
app.use('/', passport.authenticate('jwt', { session: false }), secure);

//Handling errors
app.use((error, req, res, next) => {
  console.error(`${req.method} ${req.url} ${error.message}`);
  console.error(`Error Trace:`, error);
  try {
    return res.status(500).send({ errors: error.message });
  } catch (error) {
    return next(error);
  }
});

module.exports = app;
