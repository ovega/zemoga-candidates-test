'use strict';

const path = require('path');
require('dotenv').config({ path: `${path.dirname(__dirname)}/.env` });
const mongo = require('./helpers/mongo');
const app = require('./app');

const {
  NODE_ENV,
  PORT,
  MONGO_HOST,
  MONGO_USER,
  MONGO_PASSWORD,
  MONGO_SSL,
  MONGO_AUTH_SOURCE,
  SECRET_KEY,
} = process.env;

if (
  NODE_ENV &&
  PORT &&
  MONGO_HOST &&
  MONGO_USER &&
  MONGO_PASSWORD &&
  MONGO_SSL &&
  MONGO_AUTH_SOURCE &&
  SECRET_KEY
) {
  start();
} else {
  console.error(
    'You must set the enviroment variables before running the api. \n'
  );
}

async function start() {
  await mongo.connect();
  if (mongo.isConnected()) {
    app.listen(PORT, () => {
      console.log(
        `Listening on port ${PORT} running on ${NODE_ENV} environment \n`
      );
    });
  } else {
    console.error(
      `MongoDB connection couldn't be stablished, check your configuration and try to launch the api again. \n`
    );
  }
}
