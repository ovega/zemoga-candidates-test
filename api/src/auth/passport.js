const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const UserModel = require('../models/mongo/User');
const { SECRET_KEY } = process.env;

passport.use(
  'token',
  new localStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email, password, done) => {
      try {
        const user = await UserModel.findOne({ email });
        if (!user) {
          return done(null, null, {
            message: 'User or password are not valid.',
            status: 404,
          });
        }
        const validate = await user.isValidPassword(password);
        if (!validate) {
          return done(null, null, {
            message: 'User or password are not valid.',
            status: 400,
          });
        }
        return done(null, user, null);
      } catch (err) {
        console.error(err);
        return done(err, null, {
          message: 'Internal Server Error.',
          status: 500,
        });
      }
    }
  )
);

passport.use(
  new JWTstrategy(
    {
      secretOrKey: SECRET_KEY,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        //Pass the user details to the next middleware
        return done(null, token.user);
      } catch (error) {
        done(error);
      }
    }
  )
);
